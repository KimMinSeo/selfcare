package com.health.selfcare.page.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.health.selfcare.page.dao.IPageDao;
import com.health.selfcare.page.service.impl.PageServiceImpl;

@Service
public class PageService implements PageServiceImpl {

	@Autowired
	private IPageDao IPD;

	@Override
	public List<Map<String, Object>> getExcelDataList() {
		return IPD.getExcelDataList();
	}

	@Override
	public List<Map<String, Object>> getExcelData() {
		// TODO Auto-generated method stub
		return IPD.getExcelData();
	}

	@Override
	public void cntUpdate(Map<String, Object> upMap) {
		// TODO Auto-generated method stub
	}
}
