package com.health.selfcare.page.dao;

import java.util.*;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IPageDao {

	
	public List<Map<String, Object>> getExcelDataList();
	public List<Map<String, Object>> getExcelData();
	public void cntUpdate(Map<String, Object> map);
}