package com.health.selfcare.page.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.health.selfcare.page.service.PageService;

@RestController
@RequestMapping("/selfcare")
public class PageController {
	
	
	@Autowired(required=false)
	private PageService PageService;

	private Logger logger = Logger.getLogger(PageController.class.toString());
	
	@RequestMapping("/main")
	public void main(Model model) {
	}
	
	@RequestMapping("/introduce")
	public String introduce() {
		return "jsp/introduce";
		
	}
	@RequestMapping("/schedule")
	public String schedule() {
		return "jsp/schedule";
	}
	
	@RequestMapping("/excel")
	public ModelAndView service() {
		
		ModelAndView mav = new ModelAndView();
	    mav.setViewName("jsp/excel");
	    return mav;
	}
	
	@RequestMapping("/excelList")
	public ModelAndView excelList() {
		
		ModelAndView mav = new ModelAndView();
	    mav.setViewName("jsp/excelList");
	    return mav;
	}
	
	@RequestMapping("/gallery")
	public ModelAndView gallery() {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsp/gallery");
		return mav;
	}
	
	@RequestMapping("/today")
	public ModelAndView today() {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("jsp/todayDateTime");
		return mav;
		
	}
	
	@RequestMapping("/ajax/excelList")
	public JSONArray ajaxExcel() {
		List<Map<String, Object>> listMap = PageService.getExcelDataList();
		
		JSONArray jsonArray = new JSONArray();

		for (Map<String, Object> map : listMap) {
			jsonArray.add(getJsonStringFromMap(map));
		}
		return jsonArray;
		
	}
	
	@RequestMapping("/ajax/excel")
	public JSONArray getExcelData() {
		List<Map<String, Object>> listMap = PageService.getExcelData();
		Map<String, Object> upMap = new HashMap<String,Object>();
		upMap.put("NUM", listMap.get(0).get("NUM"));
		
		PageService.cntUpdate(upMap);
		
		JSONArray jsonArray = new JSONArray();

		for (Map<String, Object> map : listMap) {
			jsonArray.add(getJsonStringFromMap(map));
		}
		return jsonArray;
		
	}
	
	
	public static String getJsonStringFromMap(Map<String, Object> map) {

		JSONObject json = new JSONObject();

		for (Map.Entry<String, Object> entry : map.entrySet()) {
			json.put(entry.getKey(), entry.getValue());
		}

		return json.toJSONString();
	}
}
