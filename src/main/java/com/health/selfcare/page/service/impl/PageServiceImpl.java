package com.health.selfcare.page.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface PageServiceImpl {
	
	public List<Map<String, Object>> getExcelDataList();
	public List<Map<String, Object>> getExcelData();
	public void cntUpdate(Map<String, Object> map);
}
