<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Health & Fitness | Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="/template-settings/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="/template-settings/css/bootstrap.min.css">
	<link rel="stylesheet" href="/template-settings/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/template-settings/css/slicknav.css">
    <link rel="stylesheet" href="/template-settings/css/flaticon.css">
    <link rel="stylesheet" href="/template-settings/css/gijgo.css">
    <link rel="stylesheet" href="/template-settings/css/animate.min.css">
    <link rel="stylesheet" href="/template-settings/css/animated-headline.css">
	<link rel="stylesheet" href="/template-settings/css/magnific-popup.css">
	<link rel="stylesheet" href="/template-settings/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="/template-settings/css/themify-icons.css">
	<link rel="stylesheet" href="/template-settings/css/slick.css">
	<link rel="stylesheet" href="/template-settings/css/nice-select.css">
	<link rel="stylesheet" href="/template-settings/css/style.css">
</head>
<body>
   <table id = "minseoTable" border="1" style="width:390px;font-size: 12px">
   		<tr>
		    <th style="width : 30px">번호</th>
			<th style="width : 250px">문제</th>
			<th style="width : 150px">정답</th> 
		</tr>                    
    </table>
    <div style="width : 400px;">
	    <div style="width:390px;">
	    	<div style="float:right">
	    		<button style="width:390px; font-size: 20px; color:black" " id="nextQuBtn" >다음문제</button>
	    	</div>
	    </div>
    </div>
    <script src="/template-settings/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="/template-settings/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="/template-settings/js/popper.min.js"></script>
    <script src="/template-settings/js/bootstrap.min.js"></script>
    <script src="/template-settings/js/jquery.slicknav.min.js"></script>
    <script src="/template-settings/js/owl.carousel.min.js"></script>
    <script src="/template-settings/js/slick.min.js"></script>
    <script src="/template-settings/js/wow.min.js"></script>
    <script src="/template-settings/js/animated.headline.js"></script>
    <script src="/template-settings/js/jquery.magnific-popup.js"></script>
    <script src="/template-settings/js/gijgo.min.js"></script>
    <script src="/template-settings/js/jquery.nice-select.min.js"></script>
    <script src="/template-settings/js/jquery.sticky.js"></script>
    <script src="/template-settings/js/jquery.counterup.min.js"></script>
    <script src="/template-settings/js/waypoints.min.js"></script>
    <script src="/template-settings/js/jquery.countdown.min.js"></script>
    <script src="/template-settings/js/hover-direction-snake.min.js"></script>
    <script src="/template-settings/js/contact.js"></script>
    <script src="/template-settings/js/jquery.form.js"></script>
    <script src="/template-settings/js/jquery.validate.min.js"></script>
    <script src="/template-settings/js/mail-script.js"></script>
    <script src="/template-settings/js/jquery.ajaxchimp.min.js"></script>
    <script src="/template-settings/js/plugins.js"></script>
    <script src="/template-settings/js/main.js"></script>
    
	<script type="text/javascript">
	$(function(){
			var params = {
			}
		$('#nextQuBtn').click(function(){
			$.ajax({
	            type : "POST",            
	            url : "/selfcare/ajax/excel",      
	            data : params,
	            dataType : 'json',
	            success : function(data){
	            	let html = ``;
            		let NUM = JSON.parse(data[0]).NUM;
            		let QU = JSON.parse(data[0]).QU;
            		let AN = JSON.parse(data[0]).AN;

            		html += '<tr>';
            		html += '	<th style="width : 25px">번호</th>';
            		html += '	<th style="width : 250px">문제</th>';
            		html += '	<th style="width : 150px">정답</th>';
            		html += '</tr>';
	        	    html += '<tr>';
		            html +=	"    <td>"+NUM+"</td>";
		            html +=	"    <td>"+QU+"</td>";
		            html +=	"    <td>"+AN+"</td>";
		            html +=	"</tr>";
		            
	            	$('#minseoTable').html(html);
	            },
	            error : function(XMLHttpRequest, textStatus, errorThrown){ 
	                alert("통신 실패.")
	            }
	        });
		})	
	})
	</script>
    </body>
</html>